import flask_wtf
from flask_wtf.files import FileField, FileAllowed

class ImageForm(FlaskForm):
    image = FileField('Put your image here!', validators=[FileAllowed(['jpg', 'png'])])