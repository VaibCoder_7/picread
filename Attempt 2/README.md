**PicRead/Attemp 2**
------------------------------------------------------------------------------------------------------

This is a second attempt at improving our project.

Our goals are as follows:
1. Experiment with new object detection models
2. Change from Flickr8k to Flickr30k
3. Use GloVe 
4. Improve accuracy 
5. Complete UI building

------------------------------------------------------------------------------------------------------